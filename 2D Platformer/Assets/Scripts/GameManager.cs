﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum GameState
{
    Menu,
    Playing
}
public class GameManager : MonoBehaviour
{

    // Singleton
    public static GameManager instance;

    // Player
    public GameObject player;

    // Bullet prefab
    public GameObject bullet;
    public float bulletSpeed;
    public float bulletLife;
    public float fireRate;

    // Awake runs before all Starts
    private void Awake()
    {
        // Setup the singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    //  for main menu
    public GameState CurrentState = GameState.Menu;
    public void ChangeGameState(GameState newState)
    {
        CurrentState = newState;
        switch (newState)
        {
            case GameState.Menu:
                break;
            case GameState.Playing:
                //LOAD GAME SCENE HERE
                SceneManager.LoadScene("Game");
                break;
            default:
                break;
        }
    }

    public void PlayGame()
    {
        ChangeGameState(GameState.Playing);
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }
}