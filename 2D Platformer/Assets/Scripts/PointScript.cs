﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PointScript : MonoBehaviour
{
    public int points = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (points==4)
        {
            SceneManager.LoadScene("Level_2");
        }
        else if (points == 5)
        {
            SceneManager.LoadScene("Victory");
        }
    }
    private void OnGUI()
    {
        // My label for player score
        GUI.color = Color.black;
        GUI.Label(new Rect(10, 10, 100, 20), "Score : " + points);
    }
}
